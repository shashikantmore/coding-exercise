package com.gamesys;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;

public class TestGameApp {

	@SuppressWarnings("static-access")
	@Test(expected = IllegalArgumentException.class)
	public void test() {
		GameApp gameApp = new GameApp();
		gameApp.console = new FileConsole("src/test/resources/input.txt",
				"src/test/resources/output.txt");
		gameApp.main(new String[] {});
	}

	@SuppressWarnings("static-access")
	@Test
	public void test2() {
		GameApp gameApp = new GameApp();
		gameApp.console = new FileConsole("src/test/resources/input.txt",
				"src/test/resources/output.txt");
		gameApp.main(new String[] { "players.txt", "1" });
	}

	@SuppressWarnings("static-access")
	@Test
	public void test3() {
		GameApp gameApp = new GameApp();
		gameApp.console = new FileConsole("src/test/resources/input2.txt",
				"src/test/resources/output2.txt");
		gameApp.main(new String[] { "players.txt", "1" });
		Assert.assertTrue(find("src/test/resources/output2.txt",
				"Player doesn't exists among pre decided contestants"));

	}

	public static boolean find(String filePath, String searchString) {
		File f = new File(filePath);
		boolean result = false;
		Scanner in = null;
		try {
			in = new Scanner(new FileReader(f));
			while (in.hasNextLine() && !result) {
				result = in.nextLine().indexOf(searchString) >= 0;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (Exception e) { /* ignore */
			}
		}
		return result;
	}
}
