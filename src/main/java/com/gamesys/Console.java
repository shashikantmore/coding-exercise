/**
 * 
 */
package com.gamesys;

/**
 * Console to handle read and write.
 * 
 * @author shash
 * 
 */
public interface Console {
	public void write(String string);

	public String read();
}
