/**
 * 
 */
package com.gamesys.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import com.gamesys.game.exception.AmountNotValid;
import com.gamesys.game.exception.BetNotValid;

/**
 * @author shash
 * 
 */
public class Player implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1923545599545753616L;

	private String name;

	private String bet;

	private String betStatus;

	private BigDecimal betAmount = new BigDecimal(0);

	private BigDecimal winAmount = new BigDecimal(0);

	public Player(String inputString) throws AmountNotValid, BetNotValid {
		if (null == inputString) {
			throw new BetNotValid("Bet can not be made");
		}

		String[] array = inputString.split(" ");

		if (array.length == 3) {
			setName(array[0]);

			setBet(array[1]);

			setBetAmount(array[2]);
		} else {
			throw new BetNotValid("Input not valid please try again.");
		}

	}

	private void setBetAmount(String string) throws AmountNotValid {
		if (null == string) {
			throw new AmountNotValid("Bet amount can not be null");
		}

		try {
			setBetAmount(new BigDecimal(string));

		} catch (NumberFormatException nfe) {
			throw new AmountNotValid("Bet amount not valid");
		}

	}

	public Player(Player player) {
		this.name = player.getName();
		this.bet = player.getBet();
		this.betStatus = player.getBetStatus();
		this.betAmount = player.getBetAmount();
		this.winAmount = player.getWinAmount();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the bet
	 */
	public String getBet() {
		return bet;
	}

	/**
	 * @param bet
	 *            the bet to set
	 * @throws BetNotValid
	 */
	public void setBet(String bet) throws BetNotValid {
		if (!"EVEN".equalsIgnoreCase(bet) && !"ODD".equalsIgnoreCase(bet)) {
			int temp;
			try {
				temp = Integer.parseInt(bet);
			} catch (NumberFormatException nfe) {
				throw new BetNotValid(
						"Bet should be either 'even', 'odd' or number between 1 to 36");
			}
			if (1 > temp && 36 < temp) {
				throw new BetNotValid(
						"Bet should be either 'even', 'odd' or number between 1 to 36");
			}
		}
		this.bet = bet;
	}

	/**
	 * @return the betStatus
	 */
	public String getBetStatus() {
		return betStatus;
	}

	/**
	 * @param betStatus
	 *            the betStatus to set
	 */
	public void setBetStatus(String betStatus) {
		this.betStatus = betStatus;
	}

	/**
	 * @return the betAmount
	 */
	public BigDecimal getBetAmount() {
		return betAmount;
	}

	/**
	 * @param betAmount
	 *            the betAmount to set
	 */
	public void setBetAmount(BigDecimal betAmount) {
		this.betAmount = betAmount;
	}

	/**
	 * @return the winAmount
	 */
	public BigDecimal getWinAmount() {
		return winAmount;
	}

	/**
	 * @param winAmount
	 *            the winAmount to set
	 */
	public void setWinAmount(BigDecimal winAmount) {
		this.winAmount = winAmount;
	}
}
