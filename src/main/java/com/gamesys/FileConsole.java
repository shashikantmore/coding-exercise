package com.gamesys;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class FileConsole implements Console {

	private BufferedReader reader;

	private PrintStream out;

	public FileConsole(String input, String output) {
		try {
			reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(input)));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		File fileOut = new File(output);
		try {
			out = new PrintStream(fileOut);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Method to read line from console
	 * 
	 * @return
	 */
	public String read() {
		// For testing purpose we can parameterized this system.in to something
		// else.
		String s = null;
		try {
			s = reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return s;
	}

	/**
	 * Method to write string on the console
	 * 
	 * @param string
	 */
	public void write(String string) {
		out.println(string);
	}

}
