/**
 * 
 */
package com.gamesys;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Set;

import org.apache.log4j.Logger;

import com.gamesys.game.Board;
import com.gamesys.game.exception.PlayerAlreadyExistsException;

/**
 * @author shash
 * 
 */
public class GameApp {

	private static final Logger log = Logger.getLogger(GameApp.class);

	public static Console console = new SystemConsole();

	/**
	 * @param args
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) {
		log.info("Begin GameApp");
		console.write("############### Welcome to Roullete ###############");
		console.write("Please type in 'exit' to exit the game)");
		if (null == args || args.length == 0)
			throw new IllegalArgumentException(
					"Illegal argument exception occured.");
		Set<String> players = null;
		try {
			players = Bootstrap.initalize(args);
		} catch (IOException e) {
			console.write(e.getMessage());
		} catch (PlayerAlreadyExistsException e) {
			console.write(e.getMessage());
		}
		Board board = new Board(players, (args.length > 1) ? args[1] : null, console);
		board.play();
	}
}
