package com.gamesys.game;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.gamesys.Console;
import com.gamesys.domain.Player;
import com.gamesys.game.exception.AmountNotValid;
import com.gamesys.game.exception.BetNotValid;

public class Board {

	private Dice dice;

	private String delay;

	private Console console;

	private Set<String> referencePlayers;

	private Set<Player> players = new HashSet<Player>();

	private Repository repository = new Repository();

	public void play() {
		dice = new Dice();
		ScheduledExecutorService exec = Executors
				.newSingleThreadScheduledExecutor();
		exec.scheduleAtFixedRate(dice, Integer.parseInt(delay),
				Integer.parseInt(delay), TimeUnit.SECONDS);
		readPlayers();
		exec.shutdown();
	}

	private void readPlayers() {
		do {
			String inputString = console.read();
			if (null == inputString) {
				break;
			}
			if ("exit".equalsIgnoreCase(inputString)) {
				break;
			}
			Player player = null;
			try {
				if (null != inputString
						&& referencePlayers.contains(inputString.substring(0,
								inputString.indexOf(' ')).toUpperCase()))
					player = new Player(inputString);
				else
					console.write("Player doesn't exists among pre decided contestants");
			} catch (AmountNotValid e1) {
				console.write(e1.getMessage());
			} catch (BetNotValid e1) {
				console.write(e1.getMessage());
			}
			if (null != player)
				players.add(player);
			if (referencePlayers.size() == players.size() && players.size() > 0) {
				while (dice.diceIndex == 0) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				conclude(dice.diceIndex);
				console.write("\n\n");
				repository.addPlayers(players);
				printFinalResult();
				console.write("\n\n");
				players.clear();
				dice.diceIndex = 0;
			}
		} while (true);
	}

	public Board(Set<String> referencePlayers, String delay, Console console) {
		if (null == referencePlayers) {
			throw new IllegalArgumentException(
					"Game can not start without players");
		}
		if (null == delay) {
			delay = "30";
		}

		this.referencePlayers = referencePlayers;
		this.delay = delay;
		this.console = console;

	}

	private void conclude(int result) {
		console.write("Number " + result);
		boolean even = false;
		if (result % 2 == 0) {
			// Even
			even = true;
		}
		printResult(result, even);
	}

	private void printResult(int result, boolean even) {
		console.write("Player\t\tBet\t\tOutcome\t\tWinnings");
		for (Player player : players) {
			if (isNumber(player.getBet())) {
				if (result == Integer.parseInt(player.getBet())) {
					win36x(player);
				} else {
					player.setBetStatus("LOSE");
				}
			} else if (("even".equalsIgnoreCase(player.getBet()) && even)
					|| ("odd".equalsIgnoreCase(player.getBet()) && !even)) {
				win2x(player);
			} else {
				player.setBetStatus("LOSE");
			}
			console.write(player.getName() + "\t\t" + player.getBet() + "\t\t"
					+ player.getBetStatus() + "\t\t" + player.getWinAmount());
		}
	}

	private boolean isNumber(String string) {
		try {
			Integer.parseInt(string);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	private void win2x(Player player) {
		player.setBetStatus("WIN");
		player.setWinAmount(player.getBetAmount().multiply(new BigDecimal(2)));

	}

	private void win36x(Player player) {
		player.setBetStatus("WIN");
		player.setWinAmount(player.getBetAmount().multiply(new BigDecimal(36)));

	}

	private void printFinalResult() {
		console.write("Player\t\tTotal Win\t\tTotal Bet");
		for (Entry<String, List<Player>> entry : repository.getPlayers()
				.entrySet()) {
			int countOfWin = repository.getWinCount(entry.getKey());
			console.write(entry.getKey() + "\t\t" + countOfWin + "\t\t"
					+ entry.getValue().size());
		}
	}
}
