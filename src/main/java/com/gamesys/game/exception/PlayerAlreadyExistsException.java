package com.gamesys.game.exception;

public class PlayerAlreadyExistsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4305355856281279629L;

	public PlayerAlreadyExistsException(String message) {
		super(message);
	}

}
