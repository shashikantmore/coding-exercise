/**
 * 
 */
package com.gamesys.game.exception;

/**
 * @author shash
 * 
 */
public class PlayerNotFound extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6725646337751947280L;

	public PlayerNotFound(String message) {
		super(message);
	}
}
