/**
 * 
 */
package com.gamesys.game.exception;

/**
 * @author shash
 * 
 */
public class BetNotValid extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2130139632959127957L;

	public BetNotValid(String message) {
		super(message);
	}

}
