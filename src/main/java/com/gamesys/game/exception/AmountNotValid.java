package com.gamesys.game.exception;

public class AmountNotValid extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2642649602149626369L;

	public AmountNotValid(String message) {
		super(message);
	}

}
