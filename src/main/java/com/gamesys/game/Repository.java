package com.gamesys.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.gamesys.domain.Player;

/**
 * This can be any database class to store history records for users/players
 * 
 * @author shash
 * 
 */
public class Repository {

	private static Map<String, List<Player>> players = new HashMap<String, List<Player>>();

	/**
	 * @return the players
	 */
	public Map<String, List<Player>> getPlayers() {
		return players;
	}

	/**
	 * Method to add players to history records
	 * 
	 * @param currentPlayers
	 *            the players to set
	 */
	public void addPlayers(Set<Player> currentPlayers) {
		for (Player player : currentPlayers) {
			if (null == players.get(player.getName())) {
				players.put(player.getName(), new ArrayList<Player>());
			}
			players.get(player.getName()).add(new Player(player));
		}
	}

	/**
	 * Method to could number of wins
	 * 
	 * @param name
	 * @return
	 */
	public int getWinCount(String name) {
		int counter = 0;
		for (Player currentPlayer : players.get(name)) {
			if ("WIN".equals(currentPlayer.getBetStatus())) {
				counter++;
			}
		}
		return counter;
	}
}
