package com.gamesys.game;

public class Dice extends Thread {

	public volatile int diceIndex;

	@Override
	public void run() {
		diceIndex = (1 + (int) (Math.random() * 36));
	}
}
