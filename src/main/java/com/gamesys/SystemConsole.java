package com.gamesys;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SystemConsole implements Console {

	/**
	 * Method to read line from console
	 * 
	 * @return
	 */
	public String read() {
		// For testing purpose we can parameterized this system.in to something
		// else.
		BufferedReader bufferRead = new BufferedReader(new InputStreamReader(
				System.in));

		String s = null;
		try {
			s = bufferRead.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return s;
	}

	/**
	 * Method to write string on the console
	 * 
	 * @param string
	 */
	public void write(String string) {
		System.out.println(string);
	}

}
