package com.gamesys;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashSet;
import java.util.Set;

import com.gamesys.game.exception.PlayerAlreadyExistsException;

public class Bootstrap {
	public static Set<String> initalize(String[] args) throws IOException,
			PlayerAlreadyExistsException {
		Set<String> result = new HashSet<String>();

		if (null == args) {
			throw new IllegalArgumentException("Null parameter not allowed");
		}
		if (args.length > 0) {
			File playerFile = new File(args[0]);
			Reader inputStream = null;
			inputStream = new FileReader(playerFile);
			if (null != inputStream) {
				BufferedReader reader = new BufferedReader(inputStream);
				String line = null;
				while ((line = reader.readLine()) != null) {
					if (result.contains(line)) {
						reader.close();
						inputStream.close();
						throw new PlayerAlreadyExistsException("Player " + line
								+ " already in the game.");
					} else {
						result.add(line.toUpperCase());
					}
				}
				reader.close();
				inputStream.close();
			}
		}
		return result;
	}
}
